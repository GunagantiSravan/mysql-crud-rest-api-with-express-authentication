function usernameValidation(username){
    const regex = /^[a-zA-Z0-9]+$/
    return regex.test(username)
  }

function passwordValidation(password) {

    function hasMinLength (password) {
        return password.length >= 6
    }
    
    function hasNumbers (password) {
        const regex = new RegExp(/[0-9]/)
        return regex.test(password)
    }
    
    function hasSpecialCharacter (password) {
      const regex = RegExp(/[*@!#%&()^~{}<>]/)
      return regex.test(password)
    }
    
    function hasCapitalLetter (password) {
        const regex = new RegExp(/[A-Z]/)
        return regex.test(password)
    }
    
    function hasLowerCase (password) {
        const regex = new RegExp(/[a-z]/)
        return regex.test(password)
    }

    function noSpaces(password){
        return ! (/\s/g.test(password))
    }
    const isValid=noSpaces(password)&& hasMinLength(password)&& hasNumbers(password)&&hasCapitalLetter(password)&&hasLowerCase(password) && hasSpecialCharacter(password)
    if (isValid) {
        return true
    }else{
        return false
    }
}

function emailValidation(email){
    const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/
    return regex.test(email)
}

module.exports = {usernameValidation,passwordValidation,emailValidation};