const jwt=require("jsonwebtoken");
const bcrypt = require("bcrypt");
const logger=require("./logger");
const { usernameValidation, emailValidation, passwordValidation }=require("./validation");
const authentication =require('./authentication');
const db=require('./mysqlConnection');

module.exports = function(app){
  
  app.post("/signup/", async (request, response) => {
    try{  
      const {username,email,password} = request.body;

      const userQuery = `SELECT * FROM user WHERE username = '${username}' OR email = '${email}';`
      db.query(userQuery,async function(error,result){
        if (error){
          logger.error(error)
          response.status(500);
          response.send({'error':error.message});
        }else{
          const foundUser=result[0];
          if(!foundUser){
              if(!usernameValidation(username)&& username!==""){
                logger.error("username is not valid. Only characters A-Z, a-z and '-' are  acceptable.")
                response.status(400);
                response.send({message:"username is not valid. Only characters A-Z, a-z and '-' are  acceptable."})
              }else if(!emailValidation(email)){
                logger.error("enter a valid email");
                response.status(400);
                response.send({message:"enter a valid email"});
              }else if (!passwordValidation(password)) {

                response.status(400);
                response.send({message:"Password should contain min 6 characters,1-uppercase ,1-lowercase, 1-specialcharacter,1-number and no spaces"});
              }else{
                const hashedPassword = await bcrypt.hash(password, 10);
                const createUserQuery = `INSERT INTO user(username,email,password)
                VALUES('${username}','${email}','${hashedPassword}')`;
                await db.query(createUserQuery,function(error){
                  if(error){
                    logger.error(error)
                    response.status(500);
                    response.send({'error':error.message});
                  }else{
                    logger.info("User created successfully");
                    response.status(200);
                    response.send({message:"User created successfully"});
                  }
                });
                
              }
          }else{
            response.status(400);
            response.send({message:"Username or email already exists"});
          }
        }
      });
    
    }catch(error){
      response.status(500);
      response.send({"Error":error.message});
    }
  });


  
  app.post("/signin/", async (request, response) => {
    try{
      const { username,email, password } = request.body;
      let userQuery=null;
      if(!username){
        userQuery = `SELECT * FROM user WHERE email = '${email}';`
      }else{
        userQuery = `SELECT * FROM user WHERE username = '${username}';`
      }

      db.query(userQuery,async function(error,result){
        if(error){
          response.status(500);
          response.send(error);
        }else{
          const foundUser=result[0];
          if (foundUser) {
            const isPasswordCorrect = await bcrypt.compare(
              password,
              foundUser.password
            );
        
            if (isPasswordCorrect) {
              const payload = { username, email:foundUser.email};
              const accessToken = jwt.sign(payload, "SECRET_KEY",{expiresIn:'2m'});
              const refreshToken=jwt.sign(payload,"REFRESH_KEY")
              const tokenQuery=`INSERT INTO tokens(username,email,token) 
                                VALUES ('${username}','${foundUser.email}','${refreshToken}');`;
              db.query(tokenQuery,function(error){
                if(error){
                  response.status(500);
                  response.send(error);
                }else{
                  logger.info("Login Successfull");
                  response.status(200);
                  response.send({ accessToken,refreshToken });
                }
              });
              
            } else {
              response.status(400);
              response.send({message:"Invalid password"});
            }
          } else {
            response.status(400);
            response.send({message:"Invalid user"});
          }
        }
      });
        
      
    }catch(error){
      response.status(500);
      response.send({"Error":error.message});
    }
  });


  app.post('/token',(request,response)=>{
    try {
      const refreshToken = request.body.token;
      if (!refreshToken) {
          response.status(404);
          respone.send("Unauthorized user");
      } else {
          const tokenQuery=`SELECT username from tokens where token = "${refreshToken}";`
          db.query(tokenQuery,(error,result) => {
              if (error){
                response.status(502);
                response.send({error});
              }
              if (result.length!==0) {
                  jwt.verify(refreshToken,"REFRESH_KEY", (error,payload)=>{
                      if (error) {
                          response.status(401).send("Invalid Refresh Token")
                      }
                      const accessToken = jwt.sign({username:payload.username,email:payload.email}, "SECRET_KEY",{expiresIn:'2m'});
                      response.status(200);
                      response.send({accessToken})
                  })
              } else {
                  response.status(404).send("Refresh token is not found");
              }
              
          })
      }
      
    } catch (error) {
        response.status(500).send(error.message)
    }
  });

  app.post('/products', authentication, (request,response) => { 
    try {
      const {productCode,name,quantity,price} = request.body;
      if(productCode && name &&quantity && price ){
        const productQuery = `SELECT * FROM products where name = '${name}';`
          db.query(productQuery,(error,result) => {
              if (error){
                response.status(500);
                response.send(error);
              }else{
                if(result[0]) {
                  response.status(400);
                  response.send("product already exist");
          
                }else {
                  const createProductQuery = `INSERT INTO products(productCode,name,quantity,price)
                              VALUES("${productCode}","${name}",${quantity},${price});`
                  db.query(createProductQuery,(error) => {
                      if(error){
                        response.status(500);
                        response.send({"Error":error.message});
                      }else{
                        db.query(productQuery,(error,result)=>{
                          if(error){
                            response.status(500);
                            response.send(error);
                          }else{
                            response.status(200);
                            response.send({"message":`product with ID ${result[0].productId} created successfully`} )
                          }
                        })
                      }
                  });
              }
            }
          });

      }else{
        response.status(400);
        response.send({"Error":'Enter all Product details'});
      }
    
    } catch (error) {
        response.status(500).send(error.message);
    }
  });

  
  app.get('/products',authentication,(request,response) => {
    try {
      const getAllProducts = "SELECT * FROM products"
      db.query(getAllProducts,(error,result)=>{
          if(error){
            response.send(500);
            response.send({"Error":error.message});
          }else{
            response.status(200);
            response.send(result);
          }
      })
    
    } catch (error) {
        response.status(500)
        response.send(error.message)
    }
  });


  app.get('/products/:id', authentication, (request,response) => {
    try {
      const productQuery = `SELECT * FROM products where productId = ${request.params.id};`
      db.query(productQuery,(error,result) => {
          if (error){
            response.status(500);
            response.send({"Error":error.message});
          }else{
            if (result.length === 0) {
                response.status(404).send(`Product not found with this ID: ${request.params.id}`)
            } else {
                response.status(200).send(result[0]);
            }
          }
      });  
    } catch (error) {
        response.status(500).send(error.message)
    }
  });

  app.put('/products/:id', authentication, (request,response) => {
    try {
      const {productCode,name,quantity,price} = request.body
      console.log(request.body);
      console.log(productCode&&name&&quantity&&price);

      if(productCode && name && quantity && price ){
        const productQuery = `SELECT * FROM products where productId = ${parseInt(request.params.id)};`
          db.query(productQuery,(error,result) => {
              if (error){
                response.status(500);
                response.send({'Error':error.message});
              }else{
                if(result.length === 0) {
                  response.status(404).send(`Product not found with ID: ${request.params.id}`)
                }else {
                  const productUpdateQuery = `UPDATE products
                      SET 
                          productCode= "${productCode}",
                          name="${name}",
                          quantity="${quantity}",
                          price="${price}"
                      where ProductId = ${request.params.id};`
                  db.query(productUpdateQuery,(error) => {
                      if(error){
                        response.status(500);
                        response.send({"Error":error.message});
                      }else{
                        response.status(200).send(`Product with ID: ${request.params.id} is updated successfully.`);
                      }
                  });
              }
            }
          });

      }else{
        response.status(400);
        response.send({"Error":'Enter all Product details'});
      }
    
    } catch (error) {
        response.status(500).send(error.message);
    }
  });


  app.delete('/products/:id', authentication, (request,response) => {
    try {
      const productQuery = `SELECT * FROM products where productId = ${request.params.id};`
      db.query(productQuery,(error,data) => {
          if (error){
            response.status(500);
            response.send({"Error":error.message});
          }
          if (data.length === 0) {
              response.status(404).send(`Product not found with this ID: ${request.params.id}`)
          } else {
              const productDeleteQuery = `DELETE FROM products
                  where productId = ${request.params.id};`
              db.query(productDeleteQuery,(error) => {
                  if(error){
                    response.status(500);
                    response.send(error);
                  }else{
                    response.status(200).send(`Product with ID: ${request.params.id} is deleted Successfully.`);
                  }
              })
          }
      });
    } catch (error) {
      response.status(500).send(error.message)
    }
  });


  app.delete('/logout', authentication, (request,response) => {
    try {
      const tokensQuery = `DELETE FROM tokens where username ='${request.username}';`
      db.query(tokensQuery, (error) => {
          if(error){
            response.status(500);
            response.send({"Error":error.message})

          }else{
            response.status(200).send("Logout Successfull")
          }
      });
    
    } catch (error) {
        response.status(500);
        response.send(error.message);
    }
  });



}
