const jwt = require('jsonwebtoken')

const authentication = (request, response, next) => {
    let jwtToken;
    const authHeader = request.headers["authorization"];
    if (authHeader) {
      jwtToken = authHeader.split(" ")[1];
    }
  
    if (jwtToken) {
      jwt.verify(jwtToken, "SECRET_KEY", (error, payload) => {
        if (error) {
          response.status(401);
          response.send("Invalid JWT Token");
        } else {
          request.username = payload.username;
          request.email = payload.email;
          next();
        }
      });
    } else {
      response.status(404);
      response.send("Invalid JWT Token");
    }
  };

  module.exports=authentication;