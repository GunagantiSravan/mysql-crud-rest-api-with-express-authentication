const express = require("express");
const routes=require("./src/api");
const logger=require("./src/logger");
const app = express();
app.use(express.json());

routes(app);


initializeServer = () => {
 try{
    app.listen(3000, () => {
      logger.info("Server Running at http://localhost:3000/");
    });
  } catch (error) {
    logger.error(error);
    response.status(500)
    response.send({error:error.message});
    process.exit(1);
  }
};
initializeServer();